# todo.d

Make your Todos end in .d!

# What is this?

Todo.d is a filesystem and file structure for organizing todos on a PC. It is intended to work with Git or some other form of VCS to safely sync between computers.

## Features
- Command line helper application
  - POSIX shell compliance, uses #!/bin/dash by default to avoid bashisms with echo
- Fancy Terminal Colors (4-bit for now, customizable using ANSI escapes)
- User configruation using XDG data dirs
- Creation, scheduled, due date tracking
- Paragraph-long descriptions per task (!)
- Tags support
- CLI tags searching
- User extendable

## Not features
- Recurring tasks (built for a Cron job and another helper script, forthcoming before too long)
- Server software
- EverNote/Google Tasks sync
- GTD proficiency
- Real documentation (yet)

# Why?
I used Emacs Org mode for tasks, and it served my needs reasonably well, but it tied me to emacs and made it hard to integrate other software into my tasks workflow. This is flat file, built to be shell-read, so therefore it's easy to (correctly) integrate counters and other niceties into a basic WM configuration.

I have used TaskWarrior before, and have played with todo.txt, but all three solutions also shared a single file for all tasks, limiting how much I could do when adjacent lines were modified on separate computers. This is the primary motivator, but for now the task indexing isn't ready for such a use. Commit (hopefully) coming soon to support this.

